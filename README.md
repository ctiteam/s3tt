**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
**DEPRECATED**
NEW REPO 
https://bitbucket.org/ctiteam/student-timetable/src/master/


APU WS S3 Cache
===============

APU web services S3 cache (without authentication) with chalice on python3.7.
This setup AWS Cloudwatch schedule (10 minutes) and an API for manual cache
purging (GET /). Cached endpoints:

- `weektimetable`
- `intake_listing`

Usage
-----

To retrieve the cached data in `open-ws` S3 bucket.

    # replace <cache> with the cached response mentioned above
    $ curl https://s3-ap-southeast-1.amazonaws.com/open-ws/<cache>

Setup
-----

    $ python -m venv venv
    $ source venv/bin/activate
    $ pip install -r requirements.txt
    $ pip install chalice

Deployment
----------

    $ aws s3 mb s3://s3tt-src
    $ aws s3 mb s3://s3tt-prod
    $ aws s3 mb s3://open-ws
    $ aws s3api put-bucket-cors --bucket open-ws --cors-configuration file://cors.json

