import boto3
import botocore
import gzip
import hashlib
import io
from urllib.request import Request, urlopen
from chalice import Chalice, Response

service = 'https://api.apiit.edu.my'
endpoints = ('/student/weektimetable', '/student/intake_listing')
bucket = 's3-ws-test'

app = Chalice(app_name='timetable')
s3 = boto3.client('s3')


def sync(endpoint):
    request = Request(service + endpoint, headers={'accept-encoding': 'gzip'})
    with urlopen(request) as response:
        if response.getheader('content-encoding') != 'gzip':
            f = io.BytesIO()
            with gzip.GzipFile(fileobj=f, mode='wb', mtime=0) as compressed:
                compressed.write(response.read())
            data = f.getvalue()
        else:
            data = response.read()

    key = endpoint.split('/').pop()
    md5 = hashlib.md5(data).hexdigest()
    try:
        s3.head_object(Bucket=bucket, Key=key, IfNoneMatch=f'"{md5}"')
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == '304':  # Not modified
            return False
        app.log.error(e)

    s3.put_object(Bucket=bucket, Key=key, Body=data, ACL='public-read',
                  ContentType='application/json', ContentEncoding='gzip')
    return True


@app.route('/', api_key_required=True)
def index():
    """Return the number of synced."""
    fresh = [sync(endpoint) for endpoint in endpoints].count(True)
    return Response(body=f"{fresh}/{len(endpoints)}",
                    status_code=200,
                    headers={'Content-Type': 'text/plain'})


@app.schedule('rate(10 minutes)')
def every_ten_minutes(event):
    """Schedule to re-sync every 10 minutes."""
    for endpoint in endpoints:
        sync(endpoint)
